# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/devops_labels_validator'
require_relative '../../job/devops_labels_nudger_job'

module Triage
  module Workflow
    class DevopsLabelsNudger < Processor
      react_to 'issue.open'

      def applicable?
        event.from_gitlab_org_gitlab? &&
          event.by_team_member? &&
          !DevopsLabelsValidator.new(event.label_names).labels_set?
      end

      def process
        DevopsLabelsNudgerJob.perform_in(Triage::DEFAULT_ASYNC_DELAY_MINUTES, event)
      end
    end
  end
end
